#! /usr/bin/python3
# -*- coding:utf8 -*-

#  Copyright 2020. All Rights Reserved.
import logging

from scripts.add import Add


class Simple_cal(object):
    def __init__(self, logging_level=logging.INFO):
        self._logging_level = logging_level
        logging.basicConfig(format='%(asctime)s - %(message)s', level=self._logging_level)  # Sensitive

    def add(self, a: int, b: int):
        add_class = Add(logging_level=self._logging_level)
        return add_class.add(a, b)
