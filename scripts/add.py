#! /usr/bin/python3
# -*- coding:utf8 -*-

#  Copyright 2020. All Rights Reserved.
import logging


class Add(object):

    def __init__(self, logging_level=logging.INFO):
        self._logging_level = logging_level
        logging.basicConfig(format='%(asctime)s - %(message)s', level=self._logging_level)

    def add(self, a: int, b: int) -> int:
        return a + b
