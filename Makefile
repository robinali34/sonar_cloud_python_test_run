# SHELL ensures more consistent behavior between macOS and Linux.
SHELL=/bin/bash

test_reports := test_reports/py

.PHONY: *

pyut:
	rm -rf $(test_reports)
	mkdir -p $(test_reports)
	python3 -m green --run-coverage --junit-report=$(test_reports)/myproject-pytests.xml tests
	python3 -m coverage  xml -o $(test_reports)/myproject-pycoverage.xml
	python3 -m coverage  html -d $(test_reports)/myproject-pycoverage-html
	@echo "HTML code coverage report was generated in $(test_reports)/html"
	@echo "Open it with:"
	@echo "  open $(test_reports)/html/index.html"