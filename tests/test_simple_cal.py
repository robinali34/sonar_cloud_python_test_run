#! /usr/bin/python3
# -*- coding:utf8 -*-

#  Copyright 2020. All Rights Reserved.

import unittest

from scripts.simple_cal import Simple_cal


class Test_Simple_cal(unittest.TestCase):
    def test_add(self):
        simple_call_class = Simple_cal()
        self.assertEqual(6, simple_call_class.add(2, 4))
